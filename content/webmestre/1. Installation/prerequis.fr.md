---
title: Prérequis
weight: 2

---



### Prérequis indispensables

Simplasso a besoin pour fonctionner d’une machine ayant les ressources suivantes :

    Un serveur http: apache2 ou nginx font très bien l’affaire, dans le cas d’apache le module rewrite doit être activé ainsi que les htaccess
    PHP en version 5.6 minimum et avec quelques-un de ces modules intl, gd, mb_string, zip
    Un SGBD de type MYSQL, MARIADB

De nombreuses offres d’hébergement actuellement répondent au exigences minimum requises. Voila la liste de quelques hébergeurs et offres qui répondent aux critères minium requis


### Prérequis pour les fonctions avancées

#### Les fonctions de recherche géographique
Installer le paquet geos sur votre système, il permettra à la librairie PHP GEOS de faire des calculs geospatials rapides.

#### Les fonction de publipostage
Installer sur votre système wkhtmlto version QT pour transformer le HTML en PDF
Télécharger  wkhtmlto pour votre OS  sur le site du logiciel https://wkhtmltopdf.org/downloads.html
et installer le paquet xvfb sur votre système

Installer sur votre système ghostscript pour compiler les PDF

#### La fonction estampiller un document
Installer sur votre système pdftk
Pour la prise en charge des document Odt, doc, docx et consort, vous pouvez installer unoconv.
