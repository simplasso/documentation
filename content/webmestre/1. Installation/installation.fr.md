---
title: Installation
weight: 3

---



Avant de passer l’installation, avez vous lu les prérequis.


### L’installation 

Commencez par télécharger l’archive , c’est un peu costaud : 70 Mo.

Décompresser l’archive à l’endroit souhaité, arrangez vous pour que la configuration de serveur http (document root) pointe sur le répertoire ’web’ nouvellement désarchivé.

Démarrer votre navigateur web, et saississez l’adresse d’accès un https://mon.url/ pour lancer la procédure d’installation

#### Etape 1 : saisir les informations d’accès à la base de donnée

(copie d’écran)

Pour éviter de passer du temps a retrouver les informations indispensables il peut être pratique de remplir la fiche de description technique, téléchargeable ici.

#### Etape 2

Saisir les informations liées à l’association
Nom de votre association

#### Etape 3

Félicitation


### Post installation

#### Configurer le serveur d’envoi d’email

Définissez dans le fichier config/packages/swiftmailer.yaml, les paramétres vers votre serveur SMTP en capacité d’envoyer un
grands nombre de mail.

Pour plus d'information : https://symfony.com/doc/current/email.html

#### Améliorer la géolocalisation
...