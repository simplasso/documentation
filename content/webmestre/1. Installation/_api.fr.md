---
title: Activer l'API
weight: 4

---

### Pourquoi activer l'API

Pour permettre d'interconnecter d'autre application à votre instannce simplasso. Vous pourrez par exemple l'ouvrir pour 
permettre au plugin Simplasso-spip de déplyer sur votre site SPIP, un espace adhérent en quelques clics.


### Technique

Commençons par générer les clés RSA pour permettre l'authentification JWT

```
mkdir -p config/jwt 
openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
```

Ensuite il nous faut renseigner dans le fichier conf/packages/lexik_jwt_authentication.yaml

```

JWT_PASSPHRASE=XXXX A XXXX REMPLACER XXXXXXXX
```

Ensuite pour permettre l'accès à la machine nous allons modifier la section access_control dans le fichier config/packages/security.yaml,
Nous allons l'adresse IP des machines pouvant y avoir accès.

```
      - { path: ^/api/activation, roles: IS_AUTHENTICATED_ANONYMOUSLY, ips: [127.0.0.1,xxx.xxx.xxx.xxx] }
      - { path: ^/api/inscription, roles: IS_AUTHENTICATED_ANONYMOUSLY, ips: [127.0.0.1,xxx.xxx.xxx.xxx] }
      - { path: ^/api/data, roles: IS_AUTHENTICATED_ANONYMOUSLY, ips: [127.0.0.1,xxx.xxx.xxx.xxx] }
      - { path: ^/api/login, roles: IS_AUTHENTICATED_ANONYMOUSLY, ips: [127.0.0.1,xxx.xxx.xxx.xxx] }
      - { path: ^/api, roles: ROLE_ADHERENT, ips: [127.0.0.1,xxx.xxx.xxx.xxx] }
```

Pour ceux qui utilisent Apache, il ne faut pas oublier de rajouter cette regler dans le virtualhost pour permettre de faire
passer les entête authentification

```
setEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
```