---
title: Les notions de base
weight: 10
chapter: true
pre: "<b>1. </b>"
---

# Les notions de base

### Petit glossaire :

- Individu désigne une personne physique ou moral
- Membre désigne un ou des individus ayant adhérer ou payer une cotisation ou autre service.
- Service rendu désigne une adhésion, une cotisation, une vente, abonnement, un don, une perte.
- Paiement c’est le règlement d’un ou plusieurs services rendus
- Solde différence entre la valeur des services rendus et des paiements depuis deux dates déterminées dans le paramétrage.
- Entités Terme générique représentant une structure : Association, Groupement, Coopérative, Entreprise. Plusieurs entités peuvent être gérées par Simplasso. les fiches individus sont communes et des consolidations peuvent être réalisées.
- Actions spécifiques Elles sont accessible, en général, dans l’entête des listes. une liste non exhaustive est disponible ici avec une brève définition


